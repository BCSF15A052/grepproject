#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <stdbool.h>
#include <dirent.h>
#include <sys/types.h>
#include <regex.h>
#include <sys/stat.h>

#define SIZE 1024
#define INVALID_INPUT 0
#define FILE_NOT_FOUND 1
#define NULL_DIRECTORY 2
#define REGEX_COMPILE_ERROR 3
#define NO_FLAG 'z'

void checkFile(char *path, char *query, char flag);
void readStdIn();
void parseArgs(int argc, char *argv[]);
void handleError(int errorCode);
void wholeWord(char *path, char *query);
void wholeLine(char *path, char *query);
bool isWCC(char c);

static char flag;
static char query[SIZE] = "";
static char *files[SIZE] = {NULL};

int main(int argc, char *argv[]) {
   
    parseArgs(argc, argv);
   
	int i;
	i = 0;
    while(files[i] != 0) {
    	checkFile(files[i], query, flag);
    ++i;    
   }
    return 0;
}


void parseArgs(int argc, char *argv[]) {
    int i,j = 0;
    char *ptr;

    if (argc == 1)
        handleError(INVALID_INPUT);

    if(argv[argc - 1][0] == '-') //if the last argument is a flag display an error message and exit
        handleError(INVALID_INPUT);

    if (argc == 2)
        readStdIn(argv[1]);//if user enters only a query search for that query in stdin

    if(argv[argc - 1][0] == '-' || argv[argc - 2][0] == '-') //if either of the last two arguments are flags display an error message and exit
        handleError(INVALID_INPUT);

    if (argc == 3) { //no flags, only query and one filename or directory
        strcpy(query, argv[1]);
        files[0] = argv[2];
        flag = NO_FLAG;
        return;
    }

    if (argc == 4) {
        if (argv[1][0] == '-') { //if first arg is a flag the second is query and third is fileName or directory
            if (strlen(argv[1]) > 2)
                handleError(INVALID_INPUT);
            flag = argv[1][1];
            strcpy(query, argv[2]);
            files[0] = argv[3];
            files[1] = 0;
        return;
        }
        else {
            flag = NO_FLAG;
            strcpy(query, argv[1]);
            files[0] = argv[2];
            files[1] = argv[3];
            files[2] = 0;
            return;
        }
    }

     if (argv[1][0] == '-') { //if the first arg is a flag, the second arg is the query and the rest are the files to search
        if (strlen(argv[1]) > 2)
            handleError(INVALID_INPUT);
        flag = argv[1][1];
        strcpy(query, argv[2]);
        for (i = 3; i < argc; ++i) {
            files[j] = argv[i];
            ++j;
        }
        files[j+1] = 0;
        return;
    } else {
         flag = NO_FLAG;
         strcpy(query, argv[1]);
         for (i = 2; i < argc; ++i) {
            files[j] = argv[i];
            ++j;
         }
         files[j+1] = 0;
    }

    return;
}

void readStdIn(char *query) {

    char input[SIZE];
    char *searchPtr;

    while(!feof(stdin)) {
       fgets(input, SIZE - 1, stdin);
       searchPtr = strstr(input, query);
        if (searchPtr) {
            puts(input);
        }
    }
}



