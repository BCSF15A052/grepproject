lude <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void search_by_name(char *, char *);
void search_by_type(char *, char *);

int main(int argc, char * argv[]) {
	if (argc == 4) {
		if ( strcmp (argv[2], "-name") == 0)
			search_by_name (argv[1], argv[3]);
		else if ( strcmp (argv[2], "-type") == 0) {
			if ( strcmp (argv[1], ".") == 0 || strcmp (argv[1], "./") == 0)
				search_by_type (getenv("PWD"), argv[3]);
			else
				search_by_type (argv[1], argv[3]);
		}
	}
        return 0;
}

void search_by_type (char * dir, char * type) {
	struct dirent **namelist;
        int count;
        count = scandir(dir, &namelist, NULL, alphasort);
        if (count < 0) {
                fprintf(stderr, "Cannot open dir:%s\n",dir);
                return;
        }

	chdir(dir);
	struct stat info;
        char buffer[4096];
        int i = 0;
        while (i < count) {
		chdir(dir);
                stat (namelist[i]->d_name, &info);
		if (strcmp(type, "s") == 0) {
			if (S_IFSOCK & info.st_mode) {
				realpath (namelist[i]->d_name, buffer);
                                printf("%s\n", buffer);
			}
		}
                else if (strcmp(type, "l") == 0) {
                        if (S_IFLNK & info.st_mode) {
                                realpath (namelist[i]->d_name, buffer);
                                printf("%s\n", buffer);
                        }
                }
                else if (strcmp(type, "f") == 0) {
                        if (S_IFREG & info.st_mode) {
                                realpath (namelist[i]->d_name, buffer);
                                printf("%s\n", buffer);
                        }
                }
                else if (strcmp(type, "b") == 0) {
                        if (S_IFBLK & info.st_mode) {
                                realpath (namelist[i]->d_name, buffer);
                                printf("%s\n", buffer);
                        }
                }
                else if (strcmp(type, "d") == 0) {
                        if (S_IFDIR & info.st_mode) {
				if(strcmp(namelist[i]->d_name, ".") != 0 && strcmp(namelist[i]->d_name, "..") != 0){
                                	realpath (namelist[i]->d_name, buffer);
                                	printf("%s\n", buffer);
				}
                        }
                }
                else if (strcmp(type, "c") == 0) {
                        if (S_IFCHR & info.st_mode) {
                                realpath (namelist[i]->d_name, buffer);
                                printf("%s\n", buffer);
                        }
                }
                else if (strcmp(type, "p") == 0) {
                        if (S_IFIFO & info.st_mode) {
                                realpath (namelist[i]->d_name, buffer);
                                printf("%s\n", buffer);
                        }
                }
		else {
			printf("Not a valid type\n");
			exit(0);
		}

		if (S_IFDIR & info.st_mode) {
                        if (strcmp(namelist[i]->d_name, ".") != 0 && strcmp(namelist[i]->d_name, "..") != 0) {
                                realpath (namelist[i]->d_name, buffer);
                                search_by_type (buffer, type);
                        }
                }

		free (namelist[i]);
                i++;
        }
}

void search_by_name (char * dir, char * name) {
	struct dirent **namelist;
	int count;
	count = scandir(dir, &namelist, NULL, alphasort);
	if (count < 0) {
		fprintf(stderr, "Cannot open dir:%s\n",dir);
		return;
	}

	struct stat info;
	char buffer[4096];
	int i = 0;
	while (i < count) {
		chdir(dir);
		stat (namelist[i]->d_name, &info);
		if (strcmp (namelist[i]->d_name, name) == 0) {
			if (dir[0] == '.')
				printf ("./%s\n", namelist[i]->d_name);
			else if (dir[0] == '/') {
				realpath (namelist[i]->d_name, buffer);
				printf("%s\n", buffer);
			}
		}
		if (S_IFDIR & info.st_mode) {
			if (strcmp(namelist[i]->d_name, ".") != 0 && strcmp(namelist[i]->d_name, "..") != 0) {
				realpath (namelist[i]->d_name, buffer);
				search_by_name (buffer, name);
			}
		}
		free (namelist[i]);
		i++;
	}
}

